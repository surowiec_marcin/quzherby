package vege.quizherby;

/**
 * Created by Vege on 24.06.2017.
 */

public class Miasto {
    private int id;
    private String wojewodztwo;
    private String miasto;
    private int czyWojewodztwo;
    private String opisGodla;
    private String zrodlo;
    private String opisMiasta;

    public Miasto(int id, String wojewodztwo, String miasto, int czyWojewodztwo, String opisGodla, String zrodlo, String opisMiasta) {
        this.id = id;
        this.wojewodztwo = wojewodztwo;
        this.miasto = miasto;
        this.czyWojewodztwo = czyWojewodztwo;
        this.opisGodla = opisGodla;
        this.zrodlo = zrodlo;
        this.opisMiasta = opisMiasta;
    }

    public int getId() {
        return id;
    }

    public String getWojewodztwo() {
        return wojewodztwo;
    }

    public String getMiasto() {
        return miasto;
    }

    public int getCzyWojewodztwo() {
        return czyWojewodztwo;
    }

    public String getOpisGodla() {
        return opisGodla;
    }

    public String getZrodlo() {
        return zrodlo;
    }

    public String getOpisMiasta() {
        return opisMiasta;
    }
}
