package vege.quizherby;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static vege.quizherby.MainActivityFragment.getListaMiast;
import static vege.quizherby.MainActivityFragment.listOfIntegers;
import static vege.quizherby.MainActivityFragment.setId;

public class FirstSecindTime extends AppCompatActivity {

    @BindView(R.id.imageView4)ImageView imageView4;
    @BindView(R.id.imageView5)ImageView imageView5;
    @BindView(R.id.imageView7)ImageView imageView7;
    @BindView(R.id.imageView8)ImageView imageView8;
    @BindView(R.id.imageView9)ImageView imageView9;
    @BindView(R.id.imageView10)ImageView imageView10;
    @BindView(R.id.imageView11)ImageView imageView11;
    @BindView(R.id.imageView12)ImageView imageView12;
    @BindView(R.id.imageView13)ImageView imageView13;
    @BindView(R.id.imageView14)ImageView imageView14;
    @BindView(R.id.imageView15)ImageView imageView15;
    @BindView(R.id.imageView16)ImageView imageView16;
    @BindView(R.id.imageView17)ImageView imageView17;
    @BindView(R.id.imageView18)ImageView imageView18;
    @BindView(R.id.imageView19)ImageView imageView19;
    @BindView(R.id.imageView20)ImageView imageView20;
    @BindView(R.id.imageView21)ImageView imageView21;
    @BindView(R.id.imageView22)ImageView imageView22;
    @BindView(R.id.imageView23)ImageView imageView23;
    @BindView(R.id.imageView24)ImageView imageView24;

    @BindView(R.id.imageView30)ImageView imageView30;
    @BindView(R.id.imageView31)ImageView imageView31;
    @BindView(R.id.imageView32)ImageView imageView32;
    @BindView(R.id.imageView33)ImageView imageView33;
    @BindView(R.id.imageView34)ImageView imageView34;
    @BindView(R.id.imageView35)ImageView imageView35;
    @BindView(R.id.imageView36)ImageView imageView36;
    @BindView(R.id.imageView37)ImageView imageView37;
    @BindView(R.id.imageView38)ImageView imageView38;
    @BindView(R.id.imageView39)ImageView imageView39;

    protected static int width;
    protected static int height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_secind_time);
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
        height = size.y;

        setSize(imageView4);
        setSize(imageView5);
        setSize(imageView7);
        setSize(imageView8);
        setSize(imageView9);
        setSize(imageView10);
        setSize(imageView11);
        setSize(imageView12);
        setSize(imageView13);
        setSize(imageView14);
        setSize(imageView15);
        setSize(imageView16);
        setSize(imageView17);
        setSize(imageView18);
        setSize(imageView19);
        setSize(imageView20);
        setSize(imageView21);
        setSize(imageView22);
        setSize(imageView23);
        setSize(imageView24);

        setSize(imageView30);
        setSize(imageView31);
        setSize(imageView32);
        setSize(imageView33);
        setSize(imageView34);
        setSize(imageView35);
        setSize(imageView36);
        setSize(imageView37);
        setSize(imageView38);
        setSize(imageView39);

        List<ImageView> listImages = new ArrayList<>();
        listImages.add(imageView4);
        listImages.add(imageView5);
        listImages.add(imageView7);
        listImages.add(imageView8);
        listImages.add(imageView9);
        listImages.add(imageView10);
        listImages.add(imageView11);
        listImages.add(imageView12);
        listImages.add(imageView13);
        listImages.add(imageView14);
        listImages.add(imageView15);
        listImages.add(imageView16);
        listImages.add(imageView17);
        listImages.add(imageView18);
        listImages.add(imageView19);
        listImages.add(imageView20);
        listImages.add(imageView21);
        listImages.add(imageView22);
        listImages.add(imageView23);
        listImages.add(imageView24);
        listImages.add(imageView30);
        listImages.add(imageView31);
        listImages.add(imageView32);
        listImages.add(imageView33);
        listImages.add(imageView34);
        listImages.add(imageView35);
        listImages.add(imageView36);
        listImages.add(imageView37);
        listImages.add(imageView38);
        listImages.add(imageView39);

        List<String> flagList = new ArrayList<>();
        MainActivityFragment mainActivityFragment=new MainActivityFragment();

        Collections.shuffle(listOfIntegers);
        for(int i=0;i<listImages.size();i++){

            listImages.get(i).setImageResource(listOfIntegers.get(i));
        }

        clickButton();
    }
    public static void setSize(ImageView imageView){

        imageView.setMaxHeight(height*14/100);
        imageView.setMaxWidth(width*19/100);
        imageView.setMinimumHeight(height*14/100);
        imageView.setMinimumWidth(width*19/100);
        imageView.setAdjustViewBounds(true);

    }
   // @OnClick(R.id.button21)
    public void clickButton(){
        moveImage(imageView12);
        moveImage(imageView13);
        moveImage(imageView10);
        moveImage(imageView11);
        moveImage(imageView9);
        moveImage(imageView8);
        moveImage(imageView7);
        moveImage(imageView5);
        moveImage(imageView4);
        moveImage(imageView14);
        moveImage(imageView15);
        moveImage(imageView16);
        moveImage(imageView17);
        moveImage(imageView18);
        moveImage(imageView19);
        moveImage(imageView20);
        moveImage(imageView21);
        moveImage(imageView22);
        moveImage(imageView23);
        moveImage(imageView24);

        moveImage(imageView30);
        moveImage(imageView31);
        moveImage(imageView32);
        moveImage(imageView33);
        moveImage(imageView34);
        moveImage(imageView35);
        moveImage(imageView36);
        moveImage(imageView37);
        moveImage(imageView38);
        moveImage(imageView39);
    }
    private void moveImage(final ImageView imageView){
        final int timeForWait=1000;
        final double rand=Math.random();

        imageView.animate().scaleY(-.0100f).scaleX(-.0100f).rotationBy((float) (180*rand*2)).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration((long) (timeForWait*rand*15));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                imageView.animate().scaleY(-.0100f).scaleX(-.0100f).rotationBy((float) (180*rand*2)).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration((long) (timeForWait*rand));

            }
        }, timeForWait*2);
        Handler handler2 = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                imageView.animate().scaleY(-1f).scaleX(-1f).rotationBy(0).translationX(0).translationY(0).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(3000);
                Intent i = new Intent(FirstSecindTime.this, Statistics.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("EXIT", true);startActivity(i);


            }
        }, timeForWait*3);
    }
}
