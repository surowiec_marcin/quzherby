package vege.quizherby;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static vege.quizherby.MainActivityFragment.choices;

public class WyborTrudnosci extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wybor_trudnosci);
        ButterKnife.bind(this);
    }
    public void openMain(){
        Intent a = new Intent(WyborTrudnosci.this, MainActivity.class);
        a.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        a.putExtra("EXIT", true);
        startActivity(a);
    }
    @OnClick(R.id.button11)
    public void veryEasy(){
        choices=2;
        openMain();
    }
    @OnClick(R.id.button12)
    public void easy(){
        choices=4;
        openMain();
    }
    @OnClick(R.id.button13)
    public void normal(){
        choices=6;
        openMain();
    }
    @OnClick(R.id.button14)
    public void hard(){
        choices=8;
        openMain();
    }
}
