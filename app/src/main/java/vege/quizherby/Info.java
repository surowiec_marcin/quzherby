package vege.quizherby;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Info extends AppCompatActivity {

    @BindView(R.id.editText2)EditText editText2;
    @BindView(R.id.editText3)EditText editText3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        String info1, info2;
        info1="Wszystkie zdjęcia użyte w tym programie zostały pobrane ze strony: https://pl.wikipedia.org. Obrazy oznaczone napisem " +
                "'FOTOJET' zostały wykonane przy użyciu aplikacji ze strony:  https://www.fotojet.com/" +
                "\nIkonę aplikacji stanowi herb miasta Sławków ";
      //  textView3.setText(info1);
        info2="Projekt i realizacja programu: www.marcinsurowiec.pl\n2017r";
        editText2.setText(info2);
        editText2.setClickable(false);
        editText3.setClickable(false);
        editText2.setKeyListener(null);
        editText3.setKeyListener(null);
        editText3.setText(info1);
    }
}
