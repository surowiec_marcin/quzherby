package vege.quizherby;

/**
 * Created by Vege on 19.07.2017.
 */

public class Scores {
    private int id;
    private int scrores;
    private int gameType;

    public Scores(int id, int scrores, int gameType) {
        this.id=id;
        this.scrores = scrores;
        this.gameType = gameType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getScrores() {
        return scrores;
    }

    public void setScrores(int scrores) {
        this.scrores = scrores;
    }

    public int getGameType() {
        return gameType;
    }

    public void setGameType(int gameType) {
        this.gameType = gameType;
    }
}
