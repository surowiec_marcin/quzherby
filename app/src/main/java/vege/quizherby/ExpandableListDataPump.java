package vege.quizherby;

/**
 * Created by Vege on 24.06.2017.
 */

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump extends AppCompatActivity{

    static Context context;

    private static List<String> miast;
    private static DatabaseHelper databaseHelper;
    private static List <String> woj;
    private static HashMap<String, List<String>> expandableListDetail;
    private static HashMap<String, List<String >> expandableImageList;
    List<String> imageList;

    public static List<String > getImageName(){
        NaukaActivity naukaActivity=new NaukaActivity();
        databaseHelper=new DatabaseHelper(DatabaseHelper.getmContext());
        woj=new ArrayList<>();
        miast=new ArrayList<>();
        woj.addAll(databaseHelper.getWojList());
        List<String> s=new ArrayList<>();

        for(int i=0;i<woj.size();i++){
            s.addAll(getExpandableListDetail().get(i));
            for(int j=0;j<s.size();j++){
                String string=woj.get(i)+"_"+getExpandableListDetail().get(i).get(j);
                string=naukaActivity.replaceChars(string);
                s.add(string);
            }
        }
        return s;
    }



    public static HashMap<String, List<String>> getData() {

        expandableListDetail = new HashMap<String, List<String>>();
        databaseHelper=new DatabaseHelper(DatabaseHelper.getmContext());
        woj=new ArrayList<>();
        miast=new ArrayList<>();

        woj.addAll(databaseHelper.getWojList());

        for(int i=0;i<woj.size();i++){

            List<String> x=new ArrayList<>();
            x.addAll(databaseHelper.getWojMiast(woj.get(i)));
            expandableListDetail.put(woj.get(i).toString(), x);
            miast.clear();
        }

        return expandableListDetail;
    }

    public static HashMap<String, List<String>> getExpandableListDetail() {
        return expandableListDetail;
    }

    public static void setExpandableListDetail(HashMap<String, List<String>> expandableListDetail) {
        ExpandableListDataPump.expandableListDetail = expandableListDetail;
    }
}