package vege.quizherby;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NaukaActivity extends AppCompatActivity {

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail;
    DatabaseHelper databaseHelper;
    @BindView(R.id.liny)LinearLayout liny;

    List<Miasto> list = new ArrayList<>();


    private ImageLoader mImageLoader;
    @BindView(R.id.imageView2)ImageView imageView2;
    @BindView(R.id.con)ConstraintLayout con;
    //@BindView(R.id.listTitle)ClipData.Item listTitle;

    private static NaukaActivity mInstance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nauka);
        ButterKnife.bind(this);
        databaseHelper = new DatabaseHelper(this);
        databaseHelper.openDatabase();
        //imageView2.setImageResource(id1);
        imageView2.setAdjustViewBounds(true);
        imageView2.setMaxHeight(FirstActivity.height*35/100);
        imageView2.setAdjustViewBounds(true);
        imageView2.setMinimumHeight(FirstActivity.height*35/100);
        //listTitle.setMaxHeight(MainActivity.height*5/100);

//        GradientDrawable gd = new GradientDrawable(
//                GradientDrawable.Orientation.TOP_BOTTOM,
//                new int[] {0x00000001,0x757575});
//
//        gd.setCornerRadius(0f);
//        liny.setBackground(gd);


        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
        expandableListDetail = ExpandableListDataPump.getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        con.setMinHeight(FirstActivity.height*65/100);
        con.setMinimumHeight(FirstActivity.height*65/100);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                String sa = getStringForImage(groupPosition, childPosition);

                sa=replaceChars(sa);

                ImageView imageView =  new ImageView(getApplicationContext());
                Context context = imageView.getContext();
                int id1 = context.getResources().getIdentifier(sa, "drawable", context.getPackageName());
                imageView.setImageResource(id1);
                imageView2.setMaxHeight(FirstActivity.height*25/100);
                imageView2.setMinimumHeight(FirstActivity.height*25/100);

                //Toast toast = new Toast(context);
                //ImageView view = new ImageView(context);
                imageView2.setImageResource(id1);
                //view.setImageResource(id1);
                //toast.setView(view);
                //toast.show();
                return false;
            }
        });
    }

    @NonNull
    public String getStringForImage(int groupPosition, int childPosition) {
        return expandableListTitle.get(groupPosition)
                            + "_"
                            + expandableListDetail.get(
                            expandableListTitle.get(groupPosition)).get(
                            childPosition);
    }

    public static String replaceChars(String str) {
        str = str.toLowerCase();
        str = str.replace('-','_');
        str = str.replace(' ','_');
        str = str.replace('ł','l');
        str = str.replace("\n", "");
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static synchronized NaukaActivity getInstance() {
        return mInstance;
    }


}
