package vege.quizherby;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static vege.quizherby.MainActivity.setRegions;

public class Woj2 extends AppCompatActivity {



    @BindView(R.id.imageView25)ImageView imageView25;
    @BindView(R.id.imageView27)ImageView imageView27;
    @BindView(R.id.imageView28)ImageView imageView28;
    @BindView(R.id.imageView29)ImageView imageView29;
    @BindView(R.id.imageView30)ImageView imageView30;
    @BindView(R.id.imageView31)ImageView imageView31;
    @BindView(R.id.imageView32)ImageView imageView32;
    @BindView(R.id.imageView33)ImageView imageView33;
    @BindView(R.id.imageView34)ImageView imageView34;
    @BindView(R.id.imageView35)ImageView imageView35;
    @BindView(R.id.imageView36)ImageView imageView36;
    @BindView(R.id.imageView37)ImageView imageView37;
    @BindView(R.id.imageView38)ImageView imageView38;
    @BindView(R.id.imageView39)ImageView imageView39;
    @BindView(R.id.imageView40)ImageView imageView40;
    @BindView(R.id.imageView41)ImageView imageView41;

    private static int x;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_woj2);
        ButterKnife.bind(this);
        x=0;
    }
    public  void choose(final ImageView imageView){
        int timeForWait=1000;
        imageView.animate().scaleY(1.00f).scaleX(-1f).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(timeForWait);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                //imageView25.animate().translationX(500).setDuration(300);
                imageView.animate().scaleY(1f).scaleX(1f).rotationBy(0).translationX(0).translationY(0).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(1000);

                Intent a = new Intent(Woj2.this, WyborTrudnosci.class);
                a.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                a.putExtra("EXIT", true);
                startActivity(a);
                if(Woj2.x==0) {
                    choose(imageView);
                    x=1;
                }
                imageView.setBackgroundColor(Color.RED);

            }
        }, timeForWait);
    }

    @OnClick(R.id.imageView27)
    public void choose27(){
        setRegions("Dolnośląskie");
        choose(imageView27);
    }
    @OnClick(R.id.imageView25)
    public void choose25(){
        setRegions("Kujawsko-Pomorskie");
        choose(imageView25);
    }
    @OnClick(R.id.imageView28)
    public void choose28(){
        setRegions("lubelskie");
        choose(imageView28);
    }
    @OnClick(R.id.imageView29)
    public void choose29(){
        setRegions("lubuskie");
        choose(imageView29);
    }
    @OnClick(R.id.imageView30)
    public void choose30(){
        setRegions("Łódzkie");
        choose(imageView30);
    }
    @OnClick(R.id.imageView31)
    public void choose31(){
        setRegions("Małopolskie");
        choose(imageView31);
    }
    @OnClick(R.id.imageView32)
    public void choose32(){
        setRegions("Mazowieckie");
        choose(imageView32);
    }
    @OnClick(R.id.imageView33)
    public void choose33(){
        setRegions("Opolskie");
        choose(imageView33);
    }
    @OnClick(R.id.imageView34)
    public void choose34(){
        setRegions("Podkarpackie");
        choose(imageView34);
    }
    @OnClick(R.id.imageView35)
    public void choose35(){
        setRegions("Podlaskie");
        choose(imageView35);
    }
    @OnClick(R.id.imageView36)
    public void choose36(){
        setRegions("Pomorskie");
        choose(imageView36);
    }
    @OnClick(R.id.imageView37)
    public void choose37(){
        setRegions("Śląskie");
        choose(imageView37);
    }
    @OnClick(R.id.imageView38)
    public void choose38(){
        setRegions("Świętokrzyskie");
        choose(imageView38);
    }
    @OnClick(R.id.imageView39)
    public void choose39(){
        setRegions("Warmińsko-Mazurskie");
        choose(imageView39);
    }
    @OnClick(R.id.imageView40)
    public void choose40(){
        setRegions("Wielkopolskie");
        choose(imageView40);
    }
    @OnClick(R.id.imageView41)
    public void choose41(){
        setRegions("Zachodniopomorskie");
        choose(imageView41);
    }

}
