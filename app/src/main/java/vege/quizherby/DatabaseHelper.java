package vege.quizherby;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Vege on 24.06.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "herby.db";
    public static final String DBLOCATION = "/data/data/vege.quizherby/databases/";
    public static Context mContext;
    private SQLiteDatabase mDatabase;

    public DatabaseHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    public static String getDBNAME() {
        return DBNAME;
    }

    public static String getDBLOCATION() {
        return DBLOCATION;
    }

    public static Context getmContext() {
        return DatabaseHelper.mContext;
    }

    public SQLiteDatabase getmDatabase() {
        return mDatabase;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DBNAME).getPath();
        if(mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public  void closeDatabase() {
        if(mDatabase!=null) {
            mDatabase.close();
        }
    }

    public List<Miasto> getListMiasto() {
        Miasto miasto = null;
        List<Miasto> miastoList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM miasto", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            miasto = new Miasto(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3),
                    cursor.getString(4),cursor.getString(5),cursor.getString(6));
            miastoList.add(miasto);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return miastoList;
    }
    public List<String> getWojList(){
        String woj=null;
        List<String> wojList=new ArrayList<>();
        openDatabase();
        Cursor cursor=mDatabase.rawQuery("SELECT DISTINCT Wojewodztwo from miasto order by Wojewodztwo;", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            woj=cursor.getString(0).toString();
            wojList.add(woj);
            cursor.moveToNext();
        }
        Collections.sort(wojList, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareToIgnoreCase(s2);
            }});
        return wojList;
    }
    public List<String> getWojMiast(String woj){
        String miast=null;
        List<String> miastList=new ArrayList<>();
        openDatabase();
        Cursor cursor=mDatabase.rawQuery("SELECT Miasto FROM miasto WHERE Wojewodztwo='"+woj+"' order by 'miasto';", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            miast=cursor.getString(0);
            miastList.add(miast);
            cursor.moveToNext();
        }
        return miastList;
    }

    public void setPoints(int point, int game){
        openDatabase();//insert into scores (score, gameType) values ('12','4');
        mDatabase.execSQL("insert into scores (score, gameType) values ('"+point+"','"+game+"');");

    }
    public int getNumberOfQuestions(){
        openDatabase();
        Cursor cursor=mDatabase.rawQuery("SELECT number from temporary;", null);
        return cursor.getInt(0);
    }
    public void setNumberOfQuestions(int i){
        openDatabase();
        mDatabase.execSQL("update temporary set 'number'='"+i+"' where id=1;", null);
    }

public List<Scores> getScores(int typeOfGame){
    Scores score=null;
    List<Scores> scores=new ArrayList<>();
    openDatabase();
    Cursor cursor=mDatabase.rawQuery("SELECT * FROM scores WHERE gameType='"+typeOfGame+"' limit 20;", null);
    cursor.moveToFirst();
    while (!cursor.isAfterLast()){
        score=new Scores(cursor.getInt(0), cursor.getInt(1),cursor.getInt(2));
        scores.add(score);
        cursor.moveToNext();
    }
    return scores;
}

}

