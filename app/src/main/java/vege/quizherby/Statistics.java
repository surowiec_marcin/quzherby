package vege.quizherby;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static vege.quizherby.MainActivityFragment.choices;
import static vege.quizherby.MainActivityFragment.totalGuesses;

import com.google.android.gms.ads.MobileAds;


public class Statistics extends AppCompatActivity {
    @BindView(R.id.textView)TextView textView;
    private String wynik;
    private double wynikProcent;

    private GraphView graph1, graph2, graph3, graph4;
    private DatabaseHelper databaseHelper;
    private List<Scores> scores2=new ArrayList<>();
    private List<Scores> scores4=new ArrayList<>();
    private List<Scores> scores6=new ArrayList<>();
    private List<Scores> scores8=new ArrayList<>();

    MainActivity mainActivity;

    @BindView(R.id.textView9)TextView textView9;
    @BindView(R.id.textView10)TextView textView10;
    @BindView(R.id.textView11)TextView textView11;
    @BindView(R.id.textView12)TextView textView12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
       //
        ButterKnife.bind(this);


        textView9.setText("Poziom trudności bardzo łatwy (dwa herby)");
        textView10.setText("Poziom trudności łatwy (cztery herby)");
        textView11.setText("Poziom trudności normalny (sześć herbów)");
        textView12.setText("Poziom trudności trudny (osiem herbów)");

        mainActivity=new MainActivity();
        databaseHelper=new DatabaseHelper(this);

        int type=choices;
        databaseHelper.setPoints(totalGuesses,type);

        wynikProcent=(MainActivityFragment.correctAnswers*100/ totalGuesses*100)/100;
        int wynikProcentInt= (int) (wynikProcent);
        wynik="Użyłeś "+ totalGuesses+" prób \nna "+MainActivityFragment.correctAnswers+" pyań znajdujących się w quizie\nCo daje "+wynikProcentInt+" procentową skuteczność";
        textView.setText(wynik);

        scores2.addAll(databaseHelper.getScores(2));
        scores4.addAll(databaseHelper.getScores(4));
        scores6.addAll(databaseHelper.getScores(6));
        scores8.addAll(databaseHelper.getScores(8));



        graph1 = (GraphView) findViewById(R.id.graph1);

        DataPoint[] dataPoints2 = new DataPoint[scores2.size()]; // declare an array of DataPoint objects with the same size as your list
        for (int i = 0; i < scores2.size(); i++) {
            // add new DataPoint object to the array for each of your list entries
            dataPoints2[i] = new DataPoint(i, (scores2.get(i).getScrores())); // not sure but I think the second argument should be of type double

        }
        LineGraphSeries<DataPoint> series2 = new LineGraphSeries<DataPoint>(dataPoints2);

        graph1.addSeries(series2);
        graph1.getViewport().setYAxisBoundsManual(true);
        graph1.getViewport().setMinY(8);
        graph1.getViewport().setMaxY(20);


        graph2 = (GraphView) findViewById(R.id.graph2);
        graph2.getViewport().setYAxisBoundsManual(true);
        graph2.getViewport().setMinY(8);
        graph2.getViewport().setMaxY(30);

        DataPoint[] dataPoints4 = new DataPoint[scores4.size()]; // declare an array of DataPoint objects with the same size as your list
        for (int i = 0; i < scores4.size(); i++) {
            // add new DataPoint object to the array for each of your list entries
            dataPoints4[i] = new DataPoint(i, (scores4.get(i).getScrores())); // not sure but I think the second argument should be of type double

        }
        LineGraphSeries<DataPoint> series4 = new LineGraphSeries<DataPoint>(dataPoints4);
        graph2.addSeries(series4);
        graph3 = (GraphView) findViewById(R.id.graph3);
        graph3.getViewport().setYAxisBoundsManual(true);
        graph3.getViewport().setMinY(8);
        graph3.getViewport().setMaxY(40);


        DataPoint[] dataPoints6 = new DataPoint[scores6.size()]; // declare an array of DataPoint objects with the same size as your list
        for (int i = 0; i < scores6.size(); i++) {
            // add new DataPoint object to the array for each of your list entries
            dataPoints6[i] = new DataPoint(i, (scores6.get(i).getScrores()));

        }
        LineGraphSeries<DataPoint> series6 = new LineGraphSeries<DataPoint>(dataPoints6);
        graph3.addSeries(series6);



        graph4 = (GraphView) findViewById(R.id.graph4);
        graph4.getViewport().setYAxisBoundsManual(true);
        graph4.getViewport().setMinY(8);
        graph4.getViewport().setMaxY(75);

        DataPoint[] dataPoints8 = new DataPoint[scores8.size()]; // declare an array of DataPoint objects with the same size as your list
        for (int i = 0; i < scores8.size(); i++) {
            // add new DataPoint object to the array for each of your list entries
            dataPoints8[i] = new DataPoint(i, (scores8.get(i).getScrores())); // not sure but I think the second argument should be of type double

        }
        LineGraphSeries<DataPoint> series8 = new LineGraphSeries<DataPoint>(dataPoints8);
        graph4.addSeries(series8);

        graph1.getViewport().setXAxisBoundsManual(true);
        graph2.getViewport().setXAxisBoundsManual(true);
        graph3.getViewport().setXAxisBoundsManual(true);
        graph4.getViewport().setXAxisBoundsManual(true);
        graph1.getViewport().setMinX(0);
        graph1.getViewport().setMaxX(20);
        graph2.getViewport().setMinX(0);
        graph2.getViewport().setMaxX(20);
        graph3.getViewport().setMinX(0);
        graph3.getViewport().setMaxX(20);
        graph4.getViewport().setMinX(0);
        graph4.getViewport().setMaxX(20);

    }
    @OnClick(R.id.button10)
    public void openAgain(){

        Intent i = new Intent(Statistics.this, FirstActivity.class);

        startActivity(i);
    }
    @OnClick(R.id.button9)
    public void closeApp(){
        //databaseHelper.close();
        finish();
        System.exit(0);android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // ustal aktualną orientację urządzenia
        int orientation = getResources().getConfiguration().orientation;

        // wyświetla menu aplikacji tylko w orientacji pionowej
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // wygeneruj menu
            getMenuInflater().inflate(R.menu.menu_info, menu);
            return true;
        }
        else
            return false;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_settings){
            Intent preferencesIntent = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(preferencesIntent);

        }
        if(item.getItemId() == R.id.action_info){
            Intent preferencesIntent = new Intent(getApplicationContext(), Info.class);
            startActivity(preferencesIntent);
        }

        return false;
    }
}
