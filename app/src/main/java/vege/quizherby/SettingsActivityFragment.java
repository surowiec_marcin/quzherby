// SettingsActivityFragment.java
// Podklasa PreferenceFragment zarządzająca ustawieniami aplikacji
package vege.quizherby;

import android.os.Bundle;
import android.preference.PreferenceFragment;

public class SettingsActivityFragment extends PreferenceFragment {
   // tworzy interfejs opcji na podstawie pliku preferences.xml znajdującego się w katalogu res/xml
   @Override
   public void onCreate(Bundle bundle) {
      super.onCreate(bundle);
      addPreferencesFromResource(R.xml.preferences); // ładuje kod XML
   }
}
