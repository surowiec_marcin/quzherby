// MainActivityFragment.java
// Zawiera kod odpowiadający za logikę aplikacji Flag Quiz
package vege.quizherby;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import static vege.quizherby.MainActivity.regions;
//import static vege.quizherby.MainActivity.wojewodztwo;
import static vege.quizherby.NaukaActivity.replaceChars;

public class MainActivityFragment extends Fragment {
   // Łańcuch używany podczas zapisywania komunikatów błędów w dzienniku.
   private static final String TAG = "QuizHerby Activity";

   private static final int FLAGS_IN_QUIZ = 10;

   protected static int numberOfClick;

   private MainActivity mainActivity;
    private NaukaActivity naukaActivity;
    static List<String> listaMiast=new ArrayList<>();



   private List<String> fileNameList; // nazwy plików flag
   private List<String> quizCountriesList; // wojewodztwo bieżącego quizu
   //private static String regionsSet; // obszary bieżącego quizu
   private String correctAnswer; // poprawna nazwa kraju przypisana do bieżącej flagi
   protected static int totalGuesses=0; // liczba prób odpowiedzi
   protected static int correctAnswers=0; // liczba poprawnych odpowiedzi
   private int guessRows; // liczba wierszy przycisków odpowiedzi wyświetlanych na ekranie
   private SecureRandom random; // obiekt używany podczas losowania
   private Handler handler; // zmienna używana podczas opóźniana ładowania kolejnej flagi
   private Animation shakeAnimation; // animacja błędnej odpowiedzi

   private LinearLayout quizLinearLayout; // rozkład zawierający quiz
   private TextView questionNumberTextView; // numer bieżącego pytania
   private ImageView imageView3;
   private LinearLayout[] guessLinearLayouts; // wiersze przycisków odpowiedzi
   private TextView answerTextView; // wyświetla poprawną odpowiedź
   private static String nazwa_woj;
   static String nazwa_zdjecia;
   private Drawable flag;
   static int idf;

   protected static List<Integer> listOfIntegers=new ArrayList<>();

    protected static Integer choices;

    static MediaPlayer mediaPlayerNo;
   static MediaPlayer mediaPlayerYes;

   static Integer liczbaPytanDoBazy;


    static DatabaseHelper databaseHelper;

    public interface MyActivityListener {
        public void sendMessage(String msg);
    }

   // konfiguruje MainActivityFragment w chwili tworzenia jego widoku
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
      super.onCreateView(inflater, container, savedInstanceState);
      View view =
         inflater.inflate(R.layout.fragment_main, container, false);
//       mainActivity.startDatabase();
      fileNameList = new ArrayList<>();
//      quizCountriesList = new ArrayList<>();
      random = new SecureRandom();
      handler = new Handler();


      // ładuje animacje trzęsienia flagąm, która jest odtwarzana po udzieleniu błędnej odpowiedzi
      shakeAnimation = AnimationUtils.loadAnimation(getActivity(),
         R.anim.incorrect_shake);
      shakeAnimation.setRepeatCount(3); // animacja jest powtarzana 3 razy

      // uzyskaj odwołania do komponentów graficznego interfejsu użytkownika
      quizLinearLayout =
         (LinearLayout) view.findViewById(R.id.quizLinearLayout);
      questionNumberTextView =
         (TextView) view.findViewById(R.id.questionNumberTextView);
      imageView3=(ImageView)view.findViewById(R.id.imageView3);
      guessLinearLayouts = new LinearLayout[4];
      guessLinearLayouts[0] =
         (LinearLayout) view.findViewById(R.id.row1LinearLayout);
      guessLinearLayouts[1] =
         (LinearLayout) view.findViewById(R.id.row2LinearLayout);
      guessLinearLayouts[2] =
         (LinearLayout) view.findViewById(R.id.row3LinearLayout);
      guessLinearLayouts[3] =
         (LinearLayout) view.findViewById(R.id.row4LinearLayout);
      answerTextView = (TextView) view.findViewById(R.id.answerTextView);
      // konfiguruje obiekty nasłuchujące przycisków odpowiedzi
      for (LinearLayout row : guessLinearLayouts) {
         for (int column = 0; column < row.getChildCount(); column++) {
            Button button = (Button) row.getChildAt(column);
            button.setOnClickListener(guessButtonListener);
         }
      }

      // określa tekst wyświetlany w polach questionNumberTextView
      questionNumberTextView.setText(
         getString(R.string.question, 1, FLAGS_IN_QUIZ));
      return view; // zwróć widok fragmentu do wyświetlenia
   }

   // aktualizuje zmienną guessRows na podstawie wartości SharedPreferences
   public void updateGuessRows(Integer integer) {


      // ustal liczbę przycisków odpowiedzi, które mają zostać wyświetlone
      //SharedPreferences.Editor editor=sharedPreferences.edit();
      //editor.putString(MainActivity.CHOICES, null);
      if(integer==null || integer==0){
         integer=4;
      }
      choices =
         integer;

         //guessRows = o / 2;
         guessRows = choices/2;
         liczbaPytanDoBazy=guessRows;

      // ukryj wszystkie obiekty LinearLayout przycisków odpowiedzi
      for (LinearLayout layout : guessLinearLayouts)
         layout.setVisibility(View.GONE);

      //  wyświetla właściwe obiekty LinearLayout przycisków odpowiedzi
      for (int row = 0; row < guessRows; row++)
         guessLinearLayouts[row].setVisibility(View.VISIBLE);
   }

   // aktualizuje obszary, które ma obejmować quiz na podstawie wartości SharedPreferences
   public void updateRegions(String region) {

      regions = region;


   }
  // public void updateRegions(String stringSet){
//       regions= stringSet;
//   }

   // przygotowuje i uruchamia kolejny quiz
   public void resetQuiz() {
      // AssetManager jest używany do uzyskiwania nazw plików obrazów flag z wybranych obszarów

      //mainActivity.onStart();

      AssetManager assets = getActivity().getAssets();
      //databaseHelper.openDatabase();

      fileNameList.clear(); // empty list of image file names

      fileNameList.addAll(getListaMiast());

      correctAnswers = 0; // resetuj liczbę poprawnych odpowiedzi
      totalGuesses = 0; // resetuj liczbę wszystkich odpowiedzi udzielonych przez użytkownika
      quizCountriesList = new ArrayList<>(); // wyczyść poprzednią listę miast

      int flagCounter = 1;

      int numberOfFlags = fileNameList.size();

      // dodaj losowe nazwy plików FLAGS_IN_QUIZ do quizCountriesList
      while (flagCounter <= FLAGS_IN_QUIZ) {
         int randomIndex = random.nextInt(numberOfFlags);

         // dodaj losowe nazwy plików FLAGS_IN_QUIZ do quizCountriesList
         String filename = fileNameList.get(randomIndex);

         // jeżeli obszar jest aktywny, ale nie został jeszcze wybrany
         if (!quizCountriesList.contains(filename)) {
            quizCountriesList.add(filename); // dodaj pliki do listy
            ++flagCounter;
         }
      }

      loadNextFlag(); // uruchom quiz, ładując pierwszą flagę
   }

   // załaduj kolejną flagę po udzieleniu przez użytkownika poprawnej odpowiedzi
   private void loadNextFlag() {
      // ustal nazwę pliku kolejnej flagi i usuń ją z listy
      String nextImage = quizCountriesList.remove(0);
      correctAnswer = nextImage; // aktualizuj poprawną odpowiedź

      answerTextView.setText(""); // wyczyść answerTextView

      // wyświetl numer bieżącego pytania
      questionNumberTextView.setText(getString(
         R.string.question, (correctAnswers + 1), FLAGS_IN_QUIZ));

      nazwa_woj=regions;
      nazwa_zdjecia=nazwa_woj+"_" + nextImage;
      nazwa_zdjecia=replaceChars(nazwa_zdjecia);
      ImageView imageView =  new ImageView(getContext());
      Context context = imageView.getContext();

      int id1 = context.getResources().getIdentifier(nazwa_zdjecia, "drawable", context.getPackageName());

      String imageUri = "vege.quizherby/res/drawable/"+nazwa_zdjecia+".png";
      setId(id1);
      listOfIntegers.add(id1);
      listOfIntegers.add(id1);
      listOfIntegers.add(id1);
      imageView3.setImageResource(id1);
      imageView3.setAdjustViewBounds(true);
      imageView3.setMaxHeight((FirstActivity.height/35)*10);

      Collections.shuffle(fileNameList); // pomieszaj nazwy plików

      // prawidłową odpowiedź umieść na końcu listy fileNameList
      int correct = fileNameList.indexOf(correctAnswer);
      fileNameList.add(fileNameList.remove(correct));

     // flagImageView.setImageDrawable(flag);

      // dodaj 2, 4, 6 lub 8 przycisków odpowiedzi w zależności od wartości zmiennej guessRows
      for (int row = 0; row < guessRows; row++) {
         // umieść przyciski w currentTableRow
         for (int column = 0;
              column < guessLinearLayouts[row].getChildCount();
              column++) {
            // uzyskaj odwołanie do przycisku w celu jego skonfigurowania
            Button newGuessButton =
               (Button) guessLinearLayouts[row].getChildAt(column);
            newGuessButton.setEnabled(true);

            // ustal nazwę miasta i przekształć ją na tekst wyświetlany w obiekcie newGuessButton
            String filename = fileNameList.get((row * 2) + column);
            newGuessButton.setText(getCountryName(filename));
         }
      }

      // zastąp losowo wybrany przycisk poprawną odpowiedzią
      int row = random.nextInt(guessRows); // pick random row
      int column = random.nextInt(2); // pick random column
      LinearLayout randomRow = guessLinearLayouts[row]; // get the row
      String countryName = getCountryName(correctAnswer);
      ((Button) randomRow.getChildAt(column)).setText(countryName);
   }

   // parsuje nazwę pliku flagi i zwraca nazwę państwa
   private String getCountryName(String name) {
      return name.substring(name.indexOf('-') + 1).replace('_', ' ');
   }

   // animacja wyświetlająca cały rozkład quizLinearLayout na ekranie lub usuwająca go z ekranu
   private void animate(boolean animateOut) {
      // zapobiegaj wyświetleniu animacji podczas umieszczania pierwszej flagi na ekranie
      if (correctAnswers == 0)
         return;

      // oblicz współrzędne x i y środka
      int centerX = (quizLinearLayout.getLeft() +
         quizLinearLayout.getRight()) / 2; // oblicz współrzędną x
      int centerY = (quizLinearLayout.getTop() +
         quizLinearLayout.getBottom()) / 2; // oblicz współrzędne y

      // oblicz promień animacji
      int radius = Math.max(quizLinearLayout.getWidth(),
         quizLinearLayout.getHeight());

      Animator animator;

      // jeżeli rozkład quizLinearLayout ma być umieszczony na ekranie, a nie z niego zdejmowany
      if (animateOut) {
         // utwórz animację okręgu odsłaniającą nowe elementy na ekranie
         animator = ViewAnimationUtils.createCircularReveal(
            quizLinearLayout, centerX, centerY, radius, 0);
         animator.addListener(
            new AnimatorListenerAdapter() {
               // wywołaj, gdy animacja się skończy
               @Override
               public void onAnimationEnd(Animator animation) {
                  loadNextFlag();
               }
            }
         );
      }
      else { // jeżeli rozkład quizLinearLayout ma zostać wyświetlony na ekranie
         animator = ViewAnimationUtils.createCircularReveal(
            quizLinearLayout, centerX, centerY, 0, radius);
      }

      animator.setDuration(500); // określ czas animacji — 500 ms
      animator.start(); // uruchom animację
   }

   // kod wykonywany po dotknięciu przycisku odpowiedzi
   private OnClickListener guessButtonListener = new OnClickListener() {
      @Override
      public void onClick(View v) {
         Button guessButton = ((Button) v);
         String guess = guessButton.getText().toString();
         String answer = getCountryName(correctAnswer);
         ++totalGuesses; // inkrementuj liczbę odpowiedzi udzielonych przez użytkownika
         numberOfClick++;

         if (guess.equals(answer)) { // jeżeli odpowiedź jest poprawna
            ++correctAnswers; // inkrementuj liczbę poprawnych odpowiedzi udzielonych przez użytkownika
            //http://url-img.link
            // poprawną odpowiedź wyświetl zieloną czcionką
            answerTextView.setText(answer + "!");
            answerTextView.setTextColor(
               getResources().getColor(R.color.correct_answer,
                  getContext().getTheme()));

            disableButtons(); // dezaktywuj wszystkie przyciski odpowiedzi

            // jeżeli użytkownik poprawnie zidentyfikował flagi FLAGS_IN_QUIZ
            if (correctAnswers == FLAGS_IN_QUIZ) {
               // DialogFragment wyświetla status quizu i uruchamia nowy quiz
               DialogFragment quizResults =
                  new DialogFragment() {
                     // utwórz okno AlertDialog i je zwróć
                     @Override
                     public Dialog onCreateDialog(Bundle bundle) {
                        AlertDialog.Builder builder =
                           new AlertDialog.Builder(getActivity());
                        builder.setMessage(
                           getString(R.string.results,
                              totalGuesses,
                              (1000 / (double) totalGuesses)));


                        // przycisk „Resetuj quiz”
                        builder.setPositiveButton(R.string.reset_quiz,
                           new DialogInterface.OnClickListener() {
                              public void onClick(DialogInterface dialog,
                                 int id) {
                                 resetQuiz();
                              }
                           }
                        );

                        return builder.create(); // zwróć AlertDialog
                     }
                  };

               // skorzystaj z FragmentManager w celu wyświetlenia okna DialogFragment
               quizResults.setCancelable(false);


               Intent i = new Intent(MainActivityFragment.super.getActivity(), FirstSecindTime.class);
               i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
               i.putExtra("EXIT", true);
               startActivity(i);
               //quizResults.show(getFragmentManager(), "quiz results");
            }
            else { // odpowiedź jest poprawna, ale quiz się jeszcze nie skończył
               // odczekaj 2 sekundy i załaduj kolejną flagę
               handler.postDelayed(
                  new Runnable() {
                     @Override
                     public void run() {
                        animate(true); // animacja usuwa flagę z ekranu
                     }
                  }, 2000); // odczekaj 2 000 milisekund (2 sekundy)
            }
            mediaPlayerYes.start();
         }
         else { // odpowiedź jest poprawna
            imageView3.startAnimation(shakeAnimation); // odtwórz animację trzęsącej się flagi

            mediaPlayerNo.start();
            // wyświetla czerwony napis „Błąd!
            answerTextView.setText(R.string.incorrect_answer);
            answerTextView.setTextColor(getResources().getColor(
               R.color.incorrect_answer, getContext().getTheme()));
            guessButton.setEnabled(false); // dezaktywuj przycisk błędnej odpowiedzi
         }
      }
   };

   // metoda narzędziowa dezaktywująca wszystkie przyciski odpowiedzi
   private void disableButtons() {
      for (int row = 0; row < guessRows; row++) {
         LinearLayout guessRow = guessLinearLayouts[row];
         for (int i = 0; i < guessRow.getChildCount(); i++)
            guessRow.getChildAt(i).setEnabled(false);
      }
   }

    public static void setListaMiast(List<String> listaMiast) {
        MainActivityFragment.listaMiast = listaMiast;
    }

   public static void setListaMiast(String nazwaWoj) {
      List<String> listaMiast2=new ArrayList<>();
      listaMiast2.addAll(databaseHelper.getWojMiast(nazwaWoj));
      MainActivityFragment.listaMiast = listaMiast2;
   }

   public static List<String> getListaMiast() {
      return listaMiast;
   }

   public static void setNazwa_zdjecia(String nazwa_zdjecia) {
      MainActivityFragment.nazwa_zdjecia = nazwa_zdjecia;
   }

   public String getNazwa_woj() {
      return nazwa_woj;
   }

   public static void setNazwa_woj(String nazwa_woj) {
      MainActivityFragment.nazwa_woj = nazwa_woj;
   }

   public static void setId(int id){
      MainActivityFragment.idf=id;
   }
   public static int getIdf(){
      return idf;
   }

   public static Integer getLiczbaPytanDoBazy() {
      return liczbaPytanDoBazy;
   }

   public static void setLiczbaPytanDoBazy(Integer liczbaPytanDoBazy) {
      MainActivityFragment.liczbaPytanDoBazy = liczbaPytanDoBazy;
   }

   public List<String> getFileNameList() {
      return fileNameList;
   }

   public void setFileNameList(List<String> fileNameList) {
      this.fileNameList = fileNameList;
   }

}

