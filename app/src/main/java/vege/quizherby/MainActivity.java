package vege.quizherby;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

import static vege.quizherby.MainActivityFragment.choices;
import static vege.quizherby.MainActivityFragment.setListaMiast;
import static vege.quizherby.NaukaActivity.replaceChars;

public class MainActivity extends AppCompatActivity implements MainActivityFragment.MyActivityListener{

    DatabaseHelper databaseHelper;
    static String regionN;
    MainActivity mainActivity;
    List<Miasto> listMiasto =new ArrayList<>();
    MainActivityFragment mainActivityFragment;
    private static boolean closeMainWindow=true;


    static MainActivity activityA;

    protected static String regions;

    public static final String CHOICES = "pref_numberOfChoices";
    public static final String REGIONY = "wybrane_regiony";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    Integer liczbapytan;

    MainActivityFragment quizFragment=new MainActivityFragment();

    static int guessRows;

    private boolean phoneDevice = true; // wymusza tryb portretowy
    private boolean preferencesChanged = true; // Czy preferencje zostały zmienione?
    static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        startDatabase();
        activityA = this;

        if(regions==null)
        {
            regions="Dolnośląskie";
        }
        setListaMiast(databaseHelper.getWojMiast(getRegions()));


        // przypisuje domyślne wartości do SharedPreferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        MainActivityFragment.numberOfClick=0;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        FirstActivity.width = size.x;
        FirstActivity.height = size.y;


        // rejestruje obiekt nasłuchujący zmian SharedPreferences
        MainActivityFragment.mediaPlayerYes = MediaPlayer.create(this, R.raw.yes);
        MainActivityFragment.mediaPlayerNo = MediaPlayer.create(this, R.raw.no);
        PreferenceManager.getDefaultSharedPreferences(this).
                registerOnSharedPreferenceChangeListener(
                        preferencesChangeListener);

        // określa rozmiar ekranu
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        // jeżeli urządzenie jest tabletem, przypisuje wartość false zmiennej phoneDevice
        if (screenSize == Configuration.SCREENLAYOUT_SIZE_LARGE ||
                screenSize == Configuration.SCREENLAYOUT_SIZE_XLARGE)
            phoneDevice = false; // urządzenie o wymiarach innych niż telefon

        // jeżeli aplikacja działa na urządzeniu mającym wymiary telefonu, to zezwalaj tylko na orientację pionową
        if (phoneDevice)
            setRequestedOrientation(
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    // wywoływana po skończeniu wykonywania metody onCreate
    @Override
    protected void onStart() {
        super.onStart();

        if (preferencesChanged) {
            // Teraz gdy domyślne preferencje zostały ustawione,
            // zainicjuj MainActivityFragment i uruchom quiz.
            MainActivityFragment quizFragment = (MainActivityFragment)
                    getSupportFragmentManager().findFragmentById(
                            R.id.quizFragment);
            quizFragment.updateGuessRows(
                    choices);
            quizFragment.updateRegions(
                    getRegions());
            quizFragment.resetQuiz();
            preferencesChanged = false;
            String i= String.valueOf(getGuessRows());
            if(closeMainWindow){
                closeMainWindow=false;
                finish();
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // ustal aktualną orientację urządzenia
        int orientation = getResources().getConfiguration().orientation;

        // wyświetla menu aplikacji tylko w orientacji pionowej
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // wygeneruj menu
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }
        else
            return false;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent i = new Intent(MainActivity.this, Woj2.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("EXIT", true);
        startActivity(i);
        return false;
    }

    // nasłuchuje zmian obiektu SharedPreferences
    private SharedPreferences.OnSharedPreferenceChangeListener preferencesChangeListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                // wywoływane, gdy użytkownik zmienia preferencje aplikacji
                @Override
                public void onSharedPreferenceChanged(
                        SharedPreferences sharedPreferences, String key) {
                    preferencesChanged = true; // użytkownik zmienił ustawienia aplikacji

                    quizFragment = (MainActivityFragment)
                            getSupportFragmentManager().findFragmentById(
                                    R.id.quizFragment);
                    String a=sharedPreferences.getString(MainActivity.CHOICES, null);
                    if(sharedPreferences!=null)
                    if (key.equals(CHOICES)) { // zmiana liczby wyświetlanych odpowiedzi
                        try {
                            editor = sharedPreferences.edit();
                            quizFragment.updateGuessRows(choices);
                            quizFragment.resetQuiz();
                        }catch (NullPointerException e){
                            System.exit(0);
                        }
                    }

                    else if (key.equals(REGIONY)) { // zmiana obszarów, których ma dotyczyć quiz
                        setRegions(sharedPreferences.getString(REGIONY, null).toString());
                        try {
                            quizFragment.resetQuiz();
                            try {
                                setListaMiast(regions);

                            } catch (NullPointerException e) {
                                //quizFragment.updateRegions(sharedPreferences);
                                System.exit(0);
                            }
                        }catch (NullPointerException e){
                            //quizFragment.updateRegions(sharedPreferences);
                            System.exit(0);
                        }
                    }
                    quizFragment.resetQuiz();
                    Toast.makeText(MainActivity.this,
                            R.string.restarting_quiz,
                            Toast.LENGTH_SHORT).show();
                    //System.exit(0);

                   }
            };


    /////Skopiowanie z bazy miast wszystkich pozycji do listy
    public List<Miasto> getListQuestions() {
        startDatabase();
        listMiasto.addAll(databaseHelper.getListMiasto());
        return listMiasto;
    }

    ////Przy pierwszym uruchomieniu skopiowanie bazy na telefon
    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DBLOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[]buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("MainActivity","DB copied");
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<String> getChoosenCities(){
        List<String> choosenCities=new ArrayList<>();
        List<Miasto> list=new ArrayList<>();
        list.addAll(getListQuestions());
        for(int i=0;i<list.size();i++) {
            String a=replaceChars(list.get(i)+"_"+list.get(i).getMiasto());
            choosenCities.add(a);
        }
        return choosenCities;
    }

    ////Wystartowanie bazy danych
    public void startDatabase(){

        databaseHelper=new DatabaseHelper(this);
        File database=getBaseContext().getDatabasePath(DatabaseHelper.getDBNAME());

        if(false == database.exists()) {
            databaseHelper.getReadableDatabase();
            //Copy db
            if(copyDatabase(this)) {
               // Toast.makeText(this, "Za chwilę nas", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Błąd kopiowania bazy danych", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    @Override
    public void sendMessage(String msg) {

    }

    public static int getGuessRows() {
        return guessRows;
    }

    public static void setGuessRows(int guessRows) {
        MainActivity.guessRows = guessRows;
    }
    public static MainActivity getInstance(){
        return   activityA;
    }

    public static String getRegions() {
        return regions;
    }

    public static void setRegions(String regions) {
        MainActivity.regions = regions;
    }
}
