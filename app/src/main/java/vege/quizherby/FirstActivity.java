package vege.quizherby;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.MobileAds;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FirstActivity extends AppCompatActivity {

    protected static int width;
    protected static int height;
    Toolbar toolbar;
    DatabaseHelper databaseHelper;
    private static boolean openNewWindow=true;
    @BindView(R.id.imageView6)ImageView imageView6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        MobileAds.initialize(this, "ca-app-pub-1487040786379130~2955814910");
        ButterKnife.bind(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if(openNewWindow){
            openNewWindow=false;
            Intent i = new Intent(FirstActivity.this, MainActivity.class);
            startActivity(i);
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // ustal aktualną orientację urządzenia
        int orientation = getResources().getConfiguration().orientation;

        // wyświetla menu aplikacji tylko w orientacji pionowej
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // wygeneruj menu
            getMenuInflater().inflate(R.menu.menu_info, menu);
            return true;
        }
        else
            return false;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

            if(item.getItemId() == R.id.action_settings){
                Intent preferencesIntent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(preferencesIntent);

            }
            if(item.getItemId() == R.id.action_info){
                Intent preferencesIntent = new Intent(getApplicationContext(), Info.class);
                startActivity(preferencesIntent);
            }

            return false;
        }


    @OnClick(R.id.button2)
    public void openMainActivity(){

        int timeForWait=3000;
        imageView6.animate().scaleY(-.0100f).scaleX(-.0100f).rotationBy(180).translationX(-500).translationY(-860).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(timeForWait);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                imageView6.animate().translationX(500).setDuration(300);
                imageView6.animate().scaleY(-1f).scaleX(-1f).rotationBy(0).translationX(0).translationY(0).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(3000);
                imageView6.setVisibility(View.VISIBLE);
                Intent i = new Intent(FirstActivity.this, Woj2.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("EXIT", true);startActivity(i);


            }
        }, timeForWait);

    }
    @OnClick(R.id.button3)
    public void openNaukaActivity(){
        Intent i = new Intent(FirstActivity.this, NaukaActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.putExtra("EXIT", true);
        startActivity(i);
    }

    public static boolean isOpenNewWindow() {
        return openNewWindow;
    }

    public static void setOpenNewWindow(boolean openNewWindow) {
        FirstActivity.openNewWindow = openNewWindow;
    }



}
