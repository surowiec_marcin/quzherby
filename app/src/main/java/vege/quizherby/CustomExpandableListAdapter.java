package vege.quizherby;

/**
 * Created by Vege on 24.06.2017.
 */

import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import static vege.quizherby.R.id.imageView;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private NaukaActivity naukaActivity;
    private List<String> expandableListTitle;
    private HashMap<String, List<String>> expandableListDetail;


    public CustomExpandableListAdapter(Context context, List<String> expandableListTitle,
                                       HashMap<String, List<String>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }
    ///// Ustawienie widoku wewnętrznej listy
    ///// Nazwa miasta oraz jego zdjęcie
    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = (String) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }


        String sa=expandableListTitle.get(listPosition)
                + "_"
                + expandableListDetail.get(
                expandableListTitle.get(listPosition)).get(expandedListPosition);

        sa=NaukaActivity.replaceChars(sa);


        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        ImageView  expandedImageView = (ImageView) convertView.
                findViewById(imageView);
        expandedListTextView.setText(expandedListText);
        int id1 = context.getResources().getIdentifier(sa, "drawable", context.getPackageName());
        expandedImageView.setImageResource(id1);
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {0x3F616261,0xBDBDBD});
        gd.setCornerRadius(0f);
        expandedListTextView.setBackground(gd);

        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    /////Ustawienia zewnętrznej listy
    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        int[] color = {Color.DKGRAY, Color.BLACK};
        float[] position = {0, 1};
        Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
//        LinearGradient lin_grad = new LinearGradient(0, 0, 0, 35,color,position, tile_mode);
//        Shader shader_gradient = lin_grad;
//        listTitleTextView.getPaint().setShader(shader_gradient);
        //listTitleTextView.setBackgroundResource(R.drawable.mybackground);
        //listTitleTextView.setHeight(10);
        GradientDrawable gd = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[] {0x2F616261,0xBDBDBD});
        gd.setCornerRadius(0f);
        listTitleTextView.setBackground(gd);


                //setAdjustViewBounds(true);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id){
        v.setBackgroundColor(Color.MAGENTA);
        return false;
    }
}

